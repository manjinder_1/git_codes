package com.hexaware.collections;
import java.util.*;


class BankAccountTreeSetOrderByAccountIdAsc implements Comparator<BankAccountTreeSet>{
	
	public int compare(BankAccountTreeSet account1,BankAccountTreeSet account2) {
		return Long.valueOf(account1.getaccountID()).compareTo(account2.getaccountID());
	}
	
}

class BankAccountTreeSetOrderByAccountIdDesc implements Comparator<BankAccountTreeSet>{
	
	public int compare(BankAccountTreeSet account1,BankAccountTreeSet account2) {
		return Long.valueOf(account2.getaccountID()).compareTo(account1.getaccountID());
	}
	
}

class BankAccountTreeSetOrderByAccountBalanceAsc implements Comparator<BankAccountTreeSet>{
	
	public int compare(BankAccountTreeSet account1,BankAccountTreeSet account2) {
		return Double.valueOf(account1.getaccountBalance()).compareTo(account2.getaccountBalance());
	}
	
}

class BankAccountTreeSetOrderByAccountBalanceDesc implements Comparator<BankAccountTreeSet>{
	
	public int compare(BankAccountTreeSet account1,BankAccountTreeSet account2) {
		return Double.valueOf(account2.getaccountBalance()).compareTo(account1.getaccountBalance());
	}
	
}


public class BankAccountTreeSetClient {
	public static void main(String[] args) {
		//Lambda Expression , which is used when there is only one method in interface 
		Comparator<BankAccountTreeSet> bankAccountTreeSetOrderByAccountBalanceDesc=(account1,account2)-> Double.valueOf(account2.getaccountBalance()).compareTo(account1.getaccountBalance());
		Set<BankAccountTreeSet> accounts1=new TreeSet<>(bankAccountTreeSetOrderByAccountBalanceDesc);//for lambda expression
		
		
		Set<BankAccountTreeSet> accounts=new TreeSet<>(new BankAccountTreeSetOrderByAccountBalanceAsc()); // this is for comparator interface
		
		//Set<BankAccountTreeSet> accounts=new TreeSet<>(); this is for comparable interface
		BankAccountTreeSet acc1=new BankAccountTreeSet(400,"Manjinder",2000);
		BankAccountTreeSet acc2=new BankAccountTreeSet(80,"Syed",50000);
		BankAccountTreeSet acc3=new BankAccountTreeSet(450,"Darshan",7000);
		BankAccountTreeSet acc4=new BankAccountTreeSet(20,"Basant",40000);
		
		
		System.out.println("For Lambda Expression");
		accounts1.add(acc1);
		accounts1.add(acc2);
		accounts1.add(acc3);
		accounts1.add(acc4);
		
		
		accounts.add(acc1);
		accounts.add(acc2);
		accounts.add(acc3);
		accounts.add(acc4);
		
		
		
		Iterator<BankAccountTreeSet> it1=accounts1.iterator();
		while(it1.hasNext()) {
			System.out.println(it1.next());
		}
		
		System.out.println("For Comparator Interface");
		Iterator<BankAccountTreeSet> it=accounts.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}

}
