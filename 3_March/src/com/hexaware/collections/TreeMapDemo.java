package com.hexaware.collections;
import java.util.*;

public class TreeMapDemo {
	public static void main(String[] args) {
		Map<Integer,String> map=new TreeMap<>();    
	      map.put(100,"Amit");    
	      map.put(102,"Ravi");    
	      map.put(101,"Vijay");    
	      map.put(103,"Rahul");    
	        
	      for(Map.Entry m:map.entrySet()){    
	       System.out.println(m.getKey()+" "+m.getValue());    
	      } 
	}
}
