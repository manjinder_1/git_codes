package com.hexaware.collections;
import java.util.List;
import java.util.ArrayList;



public class ArrayListDemo {
	public static void main(String[] args) {
		List<Integer> list=new ArrayList<>();
		list.add(34);
		list.add(74);
		list.add(34);
		
		System.out.println(list);
		Integer value=list.get(2);
		System.out.println("Value at index 2 is:"+value);
		
		boolean  contains=list.contains(74);
		System.out.println("List contains 74"+contains);
		
		list.clear();
		System.out.println("Size of list"+list.size());
	}

}
