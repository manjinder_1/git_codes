package com.hexaware.collections;

public class BankAccount
{
    private String accountHolderName;
    private double accountBalance;
    private long accountID;
	private String accountName;
    //private String message;
    private static int counter=1000;

    public BankAccount(String accountHolderName,double accountBalance){
        this.accountHolderName=accountHolderName;
        this.accountBalance=accountBalance;
        this.accountID=counter++;
    }

    public BankAccount(String accountHolderName){
        this.accountHolderName=accountHolderName;
        this.accountID=counter++;
        }

    //Constructor for hashcode() and equals()
    public BankAccount(long accountID,String accountName, double accountBalance){
    	this.accountID=accountID;
        this.accountName=accountName;
        this.accountBalance=accountBalance;
        
    }
  
    public void deposit(double amount){
        this.accountBalance=this.accountBalance+amount;
    }
/*
        public void deposit(double amount, String message){
        this.accountBalance=this.accountBalance+amount;
        this.message=message;
    }
 */
    public void withdraw(double amount){
        if(this.accountBalance>amount){
            this.accountBalance-=amount;
            //return amount;
        }
        else{
        System.out.println("Insufficient balance");
        }
    }
/*
    public void withdraw(double amount, String message){
        if(this.accountBalance>amount){
            this.accountBalance-=amount;
        }
        else{
        System.out.println("Insufficient balance");
        }
    }
*/

	public double getBalance(){
	    return this.accountBalance;
	 }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountHolderName == null) ? 0 : accountHolderName.hashCode());
		result = prime * result + (int) (accountID ^ (accountID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (accountHolderName == null) {
			if (other.accountHolderName != null)
				return false;
		} else if (!accountHolderName.equals(other.accountHolderName))
			return false;
		if (accountID != other.accountID)
			return false;
		return true;
	}

	public String getAccountHolderName(){
	    return this.accountHolderName;    
	 }
	
	public double getID(){
	    return this.accountID;
	 }
	
	public String toString() {
		return "BankAccount [accountID=" + accountID + ", accountName=" +accountName+ ",accountBalance="+accountBalance+"]";
	}
}

