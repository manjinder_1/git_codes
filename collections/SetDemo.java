package com.hexaware.collections;
import java.util.*;

import java.util.Set;

public class SetDemo {
	public static void main(String[] args) {
		Set<BankAccount> accounts=new HashSet<>();
		accounts.add(new BankAccount(1234,"Manjinder",20000));
		accounts.add(new BankAccount(12345,"Manjinder",20000));
		accounts.add(new BankAccount(12346,"Manjinder",20000));
		accounts.add(new BankAccount(12347,"Manjinder",20000));
		accounts.add(new BankAccount(12348,"Manjinder",20000));
		
		Iterator<BankAccount> it=accounts.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println(accounts.size());
		
	}

}
