package com.hexaware.collections;
import java.util.*;
public interface EmployeeBankAccount {
	void addBankAccount(BankAccount bank);
	List<BankAccount> listAllElements();
	void fetchBankAccountByID();
	void deleteBankAccountByID();

}
