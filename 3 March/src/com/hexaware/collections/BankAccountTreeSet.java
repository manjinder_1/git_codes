package com.hexaware.collections;
import java.lang.Comparable;

public class BankAccountTreeSet implements Comparable <BankAccountTreeSet>{

	private String accountHolderName;
    private double accountBalance;
    public long accountID;
    
    public BankAccountTreeSet(long accountID,String accountHolderName, double accountBalance){
    	this.accountID=accountID;
        this.accountHolderName=accountHolderName;
        this.accountBalance=accountBalance;
        
    }
    
    @Override
    public int compareTo(BankAccountTreeSet bankAccount) {
    	int result=(int)(this.accountID-bankAccount.accountID);
    	return result;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountHolderName == null) ? 0 : accountHolderName.hashCode());
		result = prime * result + (int) (accountID ^ (accountID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccountTreeSet other = (BankAccountTreeSet) obj;
		if (accountHolderName == null) {
			if (other.accountHolderName != null)
				return false;
		} else if (!accountHolderName.equals(other.accountHolderName))
			return false;
		if (accountID != other.accountID)
			return false;
		return true;
	}

	
	public String getAccountHolderName(){
	    return this.accountHolderName;    
	 }
	
	public long getaccountID(){
	    return this.accountID;
	 }
	
	public double getaccountBalance(){
	    return this.accountBalance;
	 }
	
	
	@Override
	public String toString() {
		return "BankAccountTreeSet [accountHolderName=" + accountHolderName + ", accountBalance=" + accountBalance
				+ ", accountID=" + accountID + "]";
	}
	
	
}
