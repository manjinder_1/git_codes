package com.hexaware.collections;
import java.util.*;

public class HashMapDemo {

	public static void main(String[] args) {
		BankAccountTreeSet account1=new BankAccountTreeSet(100,"Manjinder",50000);
		BankAccountTreeSet account2=new BankAccountTreeSet(100,"Rishita",60000);
		BankAccountTreeSet account3=new BankAccountTreeSet(100,"Sayed",70000);
		BankAccountTreeSet account4=new BankAccountTreeSet(100,"Simran",80000);
		
		Map<Long,BankAccountTreeSet > map=new HashMap<>();
		
		map.put(1000L, account1);
		map.put(1001L, account2);
		map.put(1002L, account3);
		map.put(1003L, account4);
	
		BankAccountTreeSet bankAccount=map.get(1002L);
		
		System.out.println(map.size());
		System.out.println("Bank account object for id 1002 is:" + bankAccount);
		
		/*Iterate through key
		 
		 Set<Long> keys=map.keySet(); //keys cannot be duplicate that's why in set
		 Iterator<Long> it=keys.iterator();
		 while(it.hasNext()) {
		 System.out.println(map.get(it.next()));
		}
		*/
		
		/* Iterate through values
		 
		Collection<BankAccountTreeSet> values=map.values();
		Iterator<BankAccountTreeSet> it=values.iterator();
		while(it.hasNext()) {
			BankAccountTreeSet account=(BankAccountTreeSet) it.next();
			System.out.println(account);
		}
		*/
		
		Set<Map.Entry<Long, BankAccountTreeSet>> entrySet=map.entrySet();
		Iterator<Map.Entry<Long, BankAccountTreeSet>> it=entrySet.iterator();
		while(it.hasNext()) {
			Map.Entry<Long, BankAccountTreeSet> entry=it.next();
			System.out.printf("Key is %s,Value is %s %n",entry.getKey(), entry.getValue());
		}
		
		
	}
}
