package com.hexaware.exceptionhandling;
import java.util.*;

public class ExceptionDemo {
	public static void main(String[] args) {
		Scanner ss=new Scanner(System.in);
		System.out.println("Enter a positive number");
		int dividend=ss.nextInt();
		
		System.out.println("Enter a divisorr");
		int divisor=ss.nextInt();
		float result=0;
		try {
		result=(dividend/divisor);
		}
		catch(ArithmeticException exception) {
			System.out.println("Please enter value greater than 0");
			System.out.println(exception);
		}
		if(result>0) {
		System.out.println("Result after division is:"+ result);
		}
		ss.close();
		
	}

}
