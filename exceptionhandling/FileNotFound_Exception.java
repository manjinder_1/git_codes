package com.hexaware.exceptionhandling;
import java.io.*;

public class FileNotFound_Exception {
	public static void main(String[] args) {
		File file =new File("D:\\Cities.txt");
		BufferedReader reader=null;
		try {
			reader=new BufferedReader(new FileReader(file));
		}catch(FileNotFoundException exception) {
			System.out.println("File with this name does nort exists");
		}
		if(reader!=null) {
			String line=null;
			boolean flag=true;
			try {
				while(flag) {
					line=reader.readLine();
					if(line==null) {
						flag=false;
					}else {
						System.out.println("City is: "+line);
					}
				}
			}catch(IOException e) {
				e.printStackTrace();
			}finally {
				if(reader!=null) {
					try {
						reader.close();
					}catch(IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
