package com.hexaware.exceptionhandling;
import java.util.*;


public class IndexOutofBound_Exception {
	public static void main(String[] args) {
	
	//int[] arr = {10,20,30,40,50};
	Scanner ss=new Scanner(System.in);
	System.out.println("How many elements you want to enter?");
	int n=ss.nextInt();
	int arr1[] = new int[n];
	System.out.println("Enter elements");
	for(int i=0;i<n;i++)
	{
		arr1[i]=ss.nextInt();
	}
	
	System.out.println("Enter index number");
	int index=ss.nextInt();
	
	try {
		int value=arr1[index];
		System.out.println("The value at index "+index+" is "+value);
	}
	catch(ArrayIndexOutOfBoundsException exception){
		System.out.println(" Please enter a value between 0 and"+ (arr1.length-1));
	}
	ss.close();
 }
}
