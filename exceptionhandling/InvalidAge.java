package com.hexaware.exceptionhandling;
import java.util.Scanner;

public class InvalidAge {
	public static void main(String[] args) {
		Scanner ss=new Scanner(System.in);
		System.out.println("Enter age:");
		int age=ss.nextInt();
		try {
			validate_age(age);
		}catch(InvalidAgeException e) {
			System.out.println(e.getMessage());
		}		
		ss.close();
	}
	public static void validate_age(int age) throws InvalidAgeException{
		if(age<18) {
			throw new InvalidAgeException("Age is less than 18");
		}
		else {
			throw new InvalidAgeException("Cast your vote");
		}
	}

}
