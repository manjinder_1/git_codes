package com.hexaware.exceptionhandling;

public class NullPointer_Exception {
	public static void main(String[] args) {
		String s=null;
		try {
		System.out.println(s.length());
		}
		catch(NullPointerException exception){
			System.out.println("Null string cannot have length");	
		}
		
	}

}
