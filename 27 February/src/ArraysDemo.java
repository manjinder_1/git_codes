
public class ArraysDemo
{
    public static void main(String[] args){
        //int [] intArray=new int[10];
        //int [] intArray=new int[]{11,22,33,44,55};
        int[] intArray={99,88,77,66,55};
        for(int index=0;index<intArray.length;index++){
            System.out.println("The value at index ["+index+"] is "+ intArray[index]);
        }
    }
}
