public class PrimitiveTypes {
    public static void main(String[] args){
        byte b=120;
        short shortValue=200;
        int intValue=30000;
        long longValue=4_00_000;
        char charValue= 'a';
        boolean flag= true;

        float floatValue=30.30f;
        double doubleValue=5255222.11;
        byte x=(byte) intValue;
        System.out.println(x+"\n");
        System.out.println("Output:\n"+b+"\n"+shortValue+"\n"+longValue+"\n"+charValue);
    }
}