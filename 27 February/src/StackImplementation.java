public class StackImplementation{
    public static void main(String[] args){
        System.out.println("Executed before method1");
        method1();
        System.out.println("Executed after method1");

    }
    public static void method1()
    {
        System.out.println("Executed before method2");
        method2();
        System.out.println("Executed after method2");
    }
    public static void method2()
    {
        System.out.println("Executed before method3");
        method3();
        System.out.println("Executed after method3");
    }
    public static void method3()
    {
        System.out.println("Executed inside method3");
    }
}