

public class Car{
    private String model="Baleno";
    private String make="Maruti";
    private int speed;
    private String color;

    public void accelerate()
    {
        speed++;
    }
    public void slowDown(){
        speed--;
    }
    public void halt(){
        speed=0;
    }
    public int getCurrentSpeed(){
    return speed;
    }
    
    @Override
    public String toString() {
    	return "{model:"+this.model+", make: "+this.make+"}";
    }
    
}