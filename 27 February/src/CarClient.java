public class CarClient
{
    public static void main(String[] args)
    {
        int i=89;
        int[] array=new int[10];
        Car mycar=new Car();
        Car mynewCar=new Car();

        mycar.accelerate();
        mycar.accelerate();
        mycar.accelerate();
        mycar.accelerate();

        mynewCar.accelerate();
        mynewCar.accelerate();
        mynewCar.accelerate();

        System.out.println("The speed of myCar is:"+ mycar.getCurrentSpeed());
        System.out.println("The speed of myCar is:"+ mynewCar.getCurrentSpeed());
        System.out.println(mynewCar);// directly with object of class 
    }
}