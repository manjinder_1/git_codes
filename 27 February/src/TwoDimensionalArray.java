
public class TwoDimensionalArray{
    public static void main(String[] args){

    /*
    int [][]arr={{1,2,3,4,5},{6,7,8,9,10}};
    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            System.out.print(arr[i][j] +" ");
        }
    System.out.println();
    }*/

    int row=4;
    int col=4;
    int initialValue=10;
    int i,j;
    int arr[][]=new int[row][col];
    populateTwoDimensionalArray(arr,initialValue);
    displayTwoDimensionalArray(arr);
    }

    public static void populateTwoDimensionalArray(int[][] arr,int initialValue){
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                arr[i][j]=initialValue++;
            }
        }

    }

    public static void displayTwoDimensionalArray(int[][] arr){
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                System.out.print(arr[i][j] +" ");
            }
            System.out.println();
        }
    }
    

    
}