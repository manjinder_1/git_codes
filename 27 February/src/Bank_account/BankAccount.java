package Bank_account;

public class BankAccount
{
    private String accountHolderName;
    private double accountBalance;
    private long accountID;
    private String message;
    private static int counter=1000;

    public BankAccount(String accountHolderName,double accountBalance){
        this.accountHolderName=accountHolderName;
        this.accountBalance=accountBalance;
        this.accountID=counter++;
    }
    public BankAccount(String accountHolderName){
        this.accountHolderName=accountHolderName;
        this.accountID=counter++;
    }
    public void deposit(double amount){
        this.accountBalance=this.accountBalance+amount;
    }

        public void deposit(double amount, String message){
        this.accountBalance=this.accountBalance+amount;
        this.message=message;
    }


    public void withdraw(double amount){
        if(this.accountBalance>amount){
            this.accountBalance-=amount;
            //return amount;
        }
        else{
        System.out.println("Insufficient balance");
        }
    }

    public void withdraw(double amount, String message){
        if(this.accountBalance>amount){
            this.accountBalance-=amount;
        }
        else{
        System.out.println("Insufficient balance");
        }
    }

    public double getBalance(){
        return this.accountBalance;
    }

    public String getAccountHolderName(){
        return this.accountHolderName;    
    }
    public double getID(){
        return this.accountID;
    }
}
