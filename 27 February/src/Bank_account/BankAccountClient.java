package Bank_account;

public class BankAccountClient{
    public static void main(String[] args){
        BankAccount acc1=new BankAccount("Manjinder",2000);
        BankAccount acc2=new BankAccount("Syed");
        //BankAccountRepository rep=new BankAccountRepository();

        System.out.println(acc1.getAccountHolderName()+" "+acc1.getBalance()+" "+acc1.getID());
        System.out.println(acc2.getAccountHolderName()+" "+acc2.getBalance()+" "+acc2.getID());
        acc2.deposit(5000,"I have deposited 5000 rupees");
        System.out.println(acc2.getAccountHolderName()+" "+acc2.getBalance()+" "+acc2.getID());

        //rep.addBankAccount(acc1);
        //rep.addBankAccount(acc2);

    }
}
