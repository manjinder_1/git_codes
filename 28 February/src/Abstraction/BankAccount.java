package Abstraction;

public abstract class BankAccount {
	long savingsBalance=50_000;
	long currentBalance=20_000;
	long salaryBalance=40_000;
	public abstract void deposit();
	public abstract void withdraw();
	
}
