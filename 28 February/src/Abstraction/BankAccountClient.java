package Abstraction;
import java.util.*;

public class BankAccountClient {
	public static void main(String[] args) {
		//int choice;
		BankAccount b1 =new Savings();
		BankAccount b2 =new Current();
		BankAccount b3 =new Salary();
		System.out.println(b1);
		System.out.println("1.Savings Account");
		System.out.println("2.Current Account");
		System.out.println("3.Salary Account");
		System.out.println("4.Exit");
		System.out.println("Enter Choice");
		Scanner ss=new Scanner(System.in);
		int choice=ss.nextInt();
		switch(choice)
		{
		case 1:
			b1.deposit();
			System.out.println("Amount in Savings Account after deposit:"+ b1.savingsBalance);
			b1.withdraw();
			System.out.println("Amount after withdrawal from Savings Account:"+ b1.savingsBalance);
			break;
		case 2:
			b2.deposit();
			System.out.println("Amount in Current Account after deposit:"+ b2.currentBalance);
			b2.withdraw();
			System.out.println("Amount after withdrawal from Current Account:"+ b2.currentBalance);
			break;
		case 3:
			b3.deposit();
			System.out.println("Amount in Salary Account after deposit:"+ b3.salaryBalance);
			b3.withdraw();
			System.out.println("Amount after withdrawal from Salary Account:"+ b3.salaryBalance);
			break;
		case 4:
			break;
		}
	}

}
	