package Abstraction;
import java.util.*;

public class Savings extends BankAccount{
	String name;
	public void  deposit() {
		System.out.println("Enter the amount to be deposited in Savings Account");
		Scanner ss=new Scanner(System.in);
		long deposit=ss.nextLong();
		if(deposit>50_000) {
			System.out.println("You need a PAN card to deposit more than 50,000");
		}
		else {
			savingsBalance+=deposit;
		}
		
	}
	public void withdraw() {
		System.out.println("Enter the amount to be withdrawn from Savings Account");
		Scanner ss=new Scanner(System.in);
		long bal=ss.nextLong();
		if(savingsBalance-bal<10_000) {
			System.out.println("You have to maintain minimum 10,000");
		}
		else {
		savingsBalance-=bal;
		}
	}
	public String toString() {
		return this.name=name;
	}

}
