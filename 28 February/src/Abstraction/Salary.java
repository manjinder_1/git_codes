package Abstraction;

import java.util.Scanner;

public class Salary extends BankAccount{
	
	public void  deposit() {
		System.out.println("Enter the amount to be deposited in Salary Account");
		Scanner ss=new Scanner(System.in);
		long deposit=ss.nextLong();
		salaryBalance+=deposit;		
	}
	public void withdraw() {
		System.out.println("Enter the amount to be withdrawn from Salary Account");
		Scanner ss=new Scanner(System.in);
		long bal=ss.nextLong();
		salaryBalance-=bal;
		}
}
