package Abstraction;

import java.util.Scanner;

public class Current extends BankAccount{
	public void  deposit() {
		System.out.println("Enter the amount to be deposited in Current Account");
		Scanner ss=new Scanner(System.in);
		long deposit=ss.nextLong();
		currentBalance+=deposit;
		
	}
	public void withdraw() {
		System.out.println("Enter the amount to be withdrawn from Current Account");
		Scanner ss=new Scanner(System.in);
		long bal=ss.nextLong();
		if(currentBalance-bal<20_000) {
			System.out.println("You have to maintain minimum 10,000");
		}
		else {
		currentBalance-=bal;
		}
	}
}
