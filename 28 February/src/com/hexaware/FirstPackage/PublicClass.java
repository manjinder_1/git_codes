package com.hexaware.FirstPackage;

public class PublicClass {
	public void publicMethod() {
		System.out.println("I am public method");
		
	}
	
	public static void greet(String name) {
		System.out.println("Hello "+ name);
		
	}
	protected void protectedMethod() {
		System.out.println("I am protected method");
		
	}
	void defaultMethod() {
		System.out.println("I am default method");
		
	}
	private void privateMethod() {
		System.out.println("I am private method");
		
	}
	

}
