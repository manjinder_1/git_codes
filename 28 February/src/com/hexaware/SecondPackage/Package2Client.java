package com.hexaware.SecondPackage;
import com.hexaware.FirstPackage.*;

import static com.hexaware.FirstPackage.PublicClass.greet; //importing static methods


public class Package2Client extends PublicClass {
	public static void main(String[] args) {
		//PublicClass obj=new PublicClass();
		Package2Client local =new Package2Client(); //with object of base class of 2nd package 
		local.protectedMethod();
		
		//if we have same class name in two different packages
		
		com.hexaware.ThirdPackage.PublicClass obj3=new com.hexaware.ThirdPackage.PublicClass(); //fully classified class
		obj3.greet();
		greet("Manjinder");
	}

}
