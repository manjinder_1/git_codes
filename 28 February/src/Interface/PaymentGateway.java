package Interface;

public interface PaymentGateway {
	void transact(String from,String to,double amount,String notes);
	
}
