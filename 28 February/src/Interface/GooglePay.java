package Interface;

public class GooglePay implements PaymentGateway, RechargeMobile{
	public void transact(String from,String to,double amount,String notes) {
		System.out.println("Initiating payment from "+from+" to "+ to +" for Rupees "+amount+ " for the purpose of " +notes);
		System.out.println("You have earned recharge points:"+50);
	}
	public void recharge(String mobile_no, double amount){
		System.out.println("Recharge successful");
	}

}
