package Inheritance;

public class OrthoPedician extends Doctor{
	
	public OrthoPedician(String name,int yearsOfExperience) {
		super(name,yearsOfExperience);
		System.out.println("Inside Constructor of OrthoPedician Class");
	}
	
	public void treatPatient() {
		conductCTScan();
		conductXRay();
	}
	
	
	public void conductCTScan() {
		System.out.println("Conducting CT Scan");
	}
	
	public void conductXRay() {
		System.out.println("Conducting XRay");
	}
    
}