package Inheritance;

public class Doctor{
    private String name;
    private int yearsOfExperience;

	public Doctor(String name,int yearsOfExperience) {
		
		this.name=name;
		this.yearsOfExperience=yearsOfExperience;
		System.out.println("Called in doctor Constructor");
		System.out.println(this.name);
	}
    public void treatPatient(){
        System.out.println("Treating Patients");

    }
    /*
    public String toString(){
    	return this.name;
    	//return this.yearsOfExperience;
    }
    */
}