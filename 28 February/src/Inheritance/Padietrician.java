package Inheritance;

public class Padietrician extends Doctor{
	
	public Padietrician(String name,int yearsOfExperience) {
		super(name,yearsOfExperience);
		System.out.println("Inside Constructor of Padietrician Class");
	}
	public void treatPatient() {
		treatKids();
	}
    
	public void treatKids(){
		System.out.println("Treating Kids");
	}
}